package kafkatools

import "strings"

func EscapeConnectorName(name string) string {
	return strings.Replace(strings.Replace(name, "-", "_", -1), ".", "_", -1)
}
